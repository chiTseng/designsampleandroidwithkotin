package com.golden.sample20190830

import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView

import com.golden.gBluetooth.GBleManager
import com.golden.gBluetooth.GBleManagerDelegate
import com.golden.iosstyle.*
import com.golden.sample.*

import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), UIViewDelegate,GBleManagerDelegate {

    var mainLayout : CoordinatorLayout? = null
    var gBlemanager : GBleManager? = null
    var textView : TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        var tv = TypedValue()
        var toolBarHeight = 0
        if (theme.resolveAttribute(R.attr.actionBarSize,tv,true))
        {
            toolBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,resources.displayMetrics)
            Log.v("yuchi","toolBar Height : " + toolBarHeight)
        }

        mainLayout = findViewById<CoordinatorLayout>(R.id.main)
        var dm = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(dm)

        Log.v("yuchi","width : " + dm.widthPixels)
        Log.v("yuchi","height : " + dm.heightPixels)


        var btn = UIButton(this,CGRect(100,toolBarHeight,200,200))
        btn.setOnClickListener(click)
        btn.setText(R.string.btn_defaut)
        mainLayout?.addView(btn,1)

        textView = findViewById(R.id.debug)
        this.notificationSample()
        this.delegateSample()
        this.singletionSample()
        this.factorySample()

        gBlemanager = GBleManager(this)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }
    private var click = View.OnClickListener {
        if (gBlemanager?.isScanning == false)
        {
            gBlemanager?.startScan()
        }
        else
        {
            gBlemanager?.stopScan()

        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    private fun notificationSample()
    {
        NotificationCenter.shared.add(this,"a",object : NotificationCenterListener {
            override fun onReceive(context: Context, message: Any?) {
                Log.v("yuchi",message?.toString())
            }
        })
        NotificationCenter.shared.post("a","yuchi Notification")
    }
    private fun delegateSample()
    {
        var view = UIView()
        view.delegate = this
        view.execute()
    }
    override fun initView()
    {
        Log.v("yuchi","initView")
    }
    override fun updateView()
    {
        Log.v("yuchi","updateView")
    }
    private fun singletionSample()
    {
        Config.shared.execute()
    }
    private fun factorySample()
    {
        val redBall = FactorySample.shard.create("red")
        redBall.execute()
        val ball =FactorySample.shard.create("")
        ball.execute()

    }
    override fun discovered(result: ScanResult?) {
        val scanRecord = Objects.requireNonNull(result?.scanRecord)?.bytes
        val hexStr = scanRecord!!.toHex()

        var hrValue = 0
        var sb = StringBuffer()
        hrValue = if (hexStr.substring(18, 24) == "475348") {
            Integer.parseInt(hexStr.substring(44, 46), 16)
        } else {
            sb  = scanRecord[27].toHrv().append(scanRecord[28].toHrv()).append(scanRecord[29].toHrv())
            Integer.valueOf(sb.toString())
        }
        var nowHR = if (hrValue >= 255) 0 else hrValue
        if (nowHR > 0){
            Log.d("yuchi", "nowHR: $nowHR")
            textView?.text = String(sb)
        }
    }
    private fun ByteArray.toHex(): String {
        return joinToString("") { "%02x".format(it) }
    }
    private fun Byte.toHrv():StringBuffer {
        var sb  = StringBuffer()
        when(String.format("%02X", this)) {
            "30"->sb.append("0")
            "31"->sb.append("1")
            "32"->sb.append("2")
            "33"->sb.append("3")
            "34"->sb.append("4")
            "35"->sb.append("5")
            "36"->sb.append("6")
            "37"->sb.append("7")
            "38"->sb.append("8")
            "39"->sb.append("9")
            else ->sb.append("0")
        }
        return sb
    }

}
