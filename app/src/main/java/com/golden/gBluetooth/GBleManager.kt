package com.golden.gBluetooth

import android.annotation.TargetApi
import android.app.Activity
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.os.Build
import android.util.Log
import java.util.*

interface GBleManagerDelegate
{
    fun discovered(result: ScanResult?)
}
class GBleManager {
    private var mTimer : Timer? = null
    private var mRestartTimer : Timer? = null

    var isScanning : Boolean = false
    var currentActivity : Activity? = null

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    constructor(activity: Activity) {
        currentActivity = activity
        if (getManager().adapter?.isEnabled == false) {
            getManager().adapter?.enable()
            Log.v("yuchi","mBleAdapter.isEnabled is " + getManager().adapter?.isEnabled)
        }
    }
    private fun getManager():BluetoothManager
    {
        return currentActivity?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    }
    fun startScan() {
        if (isScanning) { return }
        isScanning = true
        if (getManager().adapter?.isEnabled == false) {
            getManager().adapter?.enable()
            Log.v("yuchi","mBleAdapter.isEnabled is " + getManager().adapter?.isEnabled)
        }
        if (mTimer != null) {
            mTimer?.cancel()
            mTimer = null
        }

        if (mRestartTimer != null) {
            mRestartTimer?.cancel()
            mRestartTimer = null
        }
        
        mTimer = Timer()
        mTimer?.schedule(object  : TimerTask() {
            override fun run() {
                this@GBleManager.stopScan()
                this@GBleManager.restartScan()
            }
        },10000)

        val scanFilters = ArrayList<ScanFilter>()
        val filterMac = StringBuilder()
        val macArray = "E60E72EE5251".toCharArray()//03 E60E72EE5251
        for (i in macArray.indices) {
            filterMac.append(macArray[i])
            if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9)
                filterMac.append(":")
        }

        try {
            val filter = ScanFilter.Builder().setDeviceAddress(filterMac.toString()).build()
            Log.d("yuchi", "filter mac: $filterMac")
            scanFilters.add(filter)
            val mScanSettings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_OPPORTUNISTIC).setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE).setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT).build()
            getManager().adapter?.bluetoothLeScanner?.startScan(null,mScanSettings,mLeScanCallback)

        } catch (e: IllegalArgumentException) {
            Log.d("yuchi", "scanLeDevice startScan error: $e")
        } catch (e: NullPointerException) {
            Log.d("yuchi", "scanLeDevice startScan error: $e")
        } catch (e: Exception) {
            Log.d("yuchi", "scanDevice startScan error: $e")
        }
    }
    fun restartScan()
    {
        mRestartTimer = Timer()
        mRestartTimer?.schedule(object  : TimerTask() {
            override fun run() {
                this@GBleManager.startScan()
            }
        },5000)
    }
    fun stopScan() {
        Log.v("yuchi","stopScan")
        if (!isScanning) { return }
        isScanning = false

        if (mTimer != null) {
            mTimer?.cancel()
            mTimer = null
        }
        if (mRestartTimer != null) {
            mRestartTimer?.cancel()
            mRestartTimer = null
        }
        getManager().adapter?.bluetoothLeScanner?.stopScan(mLeScanCallback)

    }
    private val mLeScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            currentActivity?.runOnUiThread {
                (currentActivity as GBleManagerDelegate).discovered(result)

            }
        }
    }

}