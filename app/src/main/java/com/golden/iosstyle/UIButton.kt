package com.golden.iosstyle


import android.widget.Button
import android.content.Context
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.ShapeDrawable

import android.graphics.drawable.shapes.RoundRectShape
import android.os.Build

import android.view.ViewGroup



class UIButton(context:Context) : Button(context)
{
    var frame : CGRect = CGRect(0,0,0,0)
        set(value) {
            field = value
            var params = ViewGroup.MarginLayoutParams(field.width,field.height)
            params.topMargin = field.y
            params.leftMargin = field.x
            this.layoutParams = params
        }
    var color : Int = 0
        set(value) {
            field = value
            (this.background as ShapeDrawable).paint.color = field
        }
    constructor(context: Context,_frame:CGRect):this(context)
    {
        this.frame = _frame

        val radius : Float = 40.0f
        val outerR : FloatArray = floatArrayOf(radius, radius, radius, radius, radius, radius, radius, radius)
        var ins = RectF(frame.width / 2f,frame.height / 2f,frame.width / 2f,frame.height / 2f)
        val rr = RoundRectShape(outerR,ins,null)
        val drawable = ShapeDrawable(rr)
        drawable.paint.style = Paint.Style.FILL
        drawable.paint.strokeWidth = 2f


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.background = drawable

        }
        this.color = 0x33333333
    }


}