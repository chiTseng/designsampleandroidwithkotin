package com.golden.iosstyle

class CGRect(_x:Int, _y:Int, _width:Int, _height:Int) {
    var x  = _x
    var y = _y
    var width = _width
    var height = _height
}