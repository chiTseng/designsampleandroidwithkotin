package com.golden.iosstyle

import android.content.Context


data class Notification(var name: String, var listener: NotificationCenterListener)

interface NotificationCenterListener {
    fun onReceive(context: Context, message: Any?)
}

class NotificationCenter private constructor() {

    private var observers: HashMap<Context, Notification> = HashMap()

    companion object {
        val shared: NotificationCenter by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            NotificationCenter()

        }
    }

    fun post(name: String, message: Any? = null) {

        observers.forEach {
            if (name == it.value.name) {
                it.value.listener.onReceive(it.key, message)
            }
        }
    }

    fun add(context: Context, name: String, listener: NotificationCenterListener) {
        val notification = Notification(name, listener)
        observers.put(context, notification)
    }

    fun remove(context: Context, name: String? = null) {
        name?.let {
            val iterator = observers.entries.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next()
                if (entry.key == context && entry.value.name == it) {
                    iterator.remove()
                }
            }
            return
        }
        observers.remove(context)
    }
}