package com.golden.sample

import android.util.Log

class FactorySample {
    companion object
    {
        val shard = FactorySample()
    }
    fun create(type:String) : Ball
    {
        if (type == "red")
        {
            return RedBall()
        }
        else
        {
            return  YellowBall()
        }
    }
}

open class Ball
{

    open fun execute() {
        Log.v("yuchi","Ball")
    }
}
class RedBall : Ball()
{
    override fun execute() {
        Log.v("yuchi","Red Ball")
    }
}
class YellowBall : Ball()
{
    override fun execute() {
        Log.v("yuchi","Yellow Ball")
    }
}