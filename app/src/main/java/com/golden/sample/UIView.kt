package com.golden.sample

interface UIViewDelegate
{
    fun initView()
    fun updateView()
}
class UIView {
    var delegate : Any = Any()

    fun execute()
    {
        (delegate as UIViewDelegate).initView()
        (delegate as UIViewDelegate).updateView()
    }
}